#!/usr/bin/python
# {{{ Licence
# This file is part of SVN-Label
# SVN-label is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SVN-Label is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}


import sys
import re
import os
from pysqlite2 import dbapi2 as sqlite

def svnError(msg):
    sys.stderr.write(msg)
    sys.stderr.write("\n")    
    exit(1)

def toClient(msg):
    sys.stderr.write(msg)
    sys.stderr.write("\n")


if len(sys.argv) != 6:
    svnError("FATAL: Incorrect calling of pre-revprop-change\n")
    
revNum    = sys.argv[2]
propName  = sys.argv[4]
action    = sys.argv[5].upper()


# Deal with non-SVNlabel revprop changes
if action=="M" and propName=="svn:log":
    exit(0)


# Datafile should be located in same directory as the script. Not sure how well
# this will work on a non-unix platform..
expr = re.compile("(.+)[\\/]")
path = expr.match(sys.argv[0]).group()
dataFile = path + "svnlabel.db"
databaseFound = os.path.exists(dataFile)
conn = sqlite.connect(dataFile)
curs = conn.cursor()
        
if databaseFound == False:
    # Create new database table if dataFile doesn't exist
    curs.execute(
        "CREATE TABLE labels (rev INTEGER, label VARCHAR(32) PRIMARY KEY)")
    conn.commit()

if propName=="svnlabel:list":
    curs.execute("SELECT count(*) FROM labels")
    rowCount = int(curs.fetchone()[0])
    toClient( "OK " + str(rowCount) )
    curs.execute("SELECT * FROM labels")
    for row in curs:
        toClient( str(str(row[0]) + " " + str(row[1])) )
    
elif propName=="svnlabel:get":
    labelName = sys.stdin.readline()#[:-1]
    curs.execute(
        "SELECT count(*) FROM labels WHERE label='" + labelName + "'")
    rowCount = int(curs.fetchone()[0])
    if rowCount == 0:
        toClient( "FATAL not found '" + labelName + "'")
    else:
        curs.execute(
            "SELECT * FROM labels WHERE label='" + labelName + "'")
        toClient( "OK " + str(rowCount) )
        for row in curs:
            toClient( str(row[0]) + " " + str(row[1]))

elif propName=="svnlabel:tag":
    labelName = sys.stdin.readline()# [:-1]
    if action=="M" or action=="A":
        curs.execute("INSERT OR REPLACE INTO labels" +
                     " (rev,label) VALUES" + 
                     " ('" + revNum + "','" + labelName + "')")
    else:
        svnError("Unknown action: " + sys.argv[5])
    toClient( "OK 1" )
    conn.commit()

elif propName=="svnlabel:del":
    labelName = sys.stdin.readline()
    curs.execute("DELETE FROM labels WHERE label='" + labelName + "'")
    conn.commit()
    toClient( "OK 1" )

else:
    svnError("Only svn:log and svnlabel:tags may be changed")

conn.close()
exit(1)
