#!/usr/bin/python
# {{{ Licence
# This file is part of SVN-Label
# SVN-label is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SVN-Label is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}


import sys
import subprocess


def lookupLabel(label):
    labelUpper = label.upper()
    # Pass back built-in labels and things that are already numbers
    if labelUpper == "HEAD" or labelUpper == "BASE" or \
       labelUpper == "PREV" or labelUpper == "COMMITTED" or \
       label[0] == "{"      or label.isdigit():
        return label
    
    if labelCache.has_key(label):
        return str(labelCache[label])
    else:
        # Server callout to lookup label
        args = ["svn","propset","svnlabel:get","--revprop","-r","0",label]
        proc = subprocess.Popen( args, stderr=subprocess.PIPE )
        (out,err) = proc.communicate()
        result = err.rsplit("\n")
        if result[1] == "OK 1":
            keyval = result[2].rsplit(" ")[0]
            labelCache[label] = keyval
            return keyval
        else:
            print "Error: Label '" + label + "' not found\n"
            exit(0)
            

# Go thru args, changing labels to revision numbers. If an uncached label
# if found, a callout to the server is performed
labelCache = {}
newArgs = ["svn"]
idx = 1
while idx < len(sys.argv):
    arg = sys.argv[idx]
    if arg == "-r" or arg == "--revision":
        if idx+1 < len(sys.argv):
            newArgs.append( arg )
            newArgs.append( lookupLabel(sys.argv[idx+1]) )
            idx = idx + 1
    elif arg.startswith( "-r" ):
        newArgs.append( "-r" + lookupLabel(arg[2:]) )
    elif arg.startswith( "@" ):
        newArgs.append( "@" + lookupLabel(arg[1:]) )
    else:
        newArgs.append( arg )
    idx = idx + 1

# The "actual" svn call
subprocess.Popen( newArgs )
exit(0);
