#!/usr/bin/python
# {{{ Licence
# This file is part of SVN-Label
# SVN-label is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SVN-Label is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}

import subprocess

args = ["svn","propset","svnlabel:list","--revprop","-r","0","0"]
proc = subprocess.Popen( args, stderr=subprocess.PIPE )
(out,err) = proc.communicate()
labels = err.rsplit("\n")

if labels[1][0]=='O' and labels[1][1]=='K':
    for label in labels[2:]:
        if label != "":
            print label
