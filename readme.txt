SVN-Label: Svn Revision Labels (v2.0beta)

This program provides wrappers that allow people to give revision numbers 
more meaningful labels (e.g. rev-1.0 rather than having to remember r1372). 
It was written because in Subversion, a tag is really a branch, and a lot of 
people who used CVS previously disliked this approach. Allowing people to 
give revision numbers aliases ("labels") is a relatively easy way to emulate 
the old CVS Tag behaviour, but so far no-one had implemented it. 


Changes since v1.4
==================
This is a complete rewrite using Python (rather than the previous mix of C,
Perl and Bash shellscript) and SQLite. Main archiectural change is that the
client-side wrapper does one callout per unique label rather than fetching
all the labels and doing local lookups.


Requirements
============
This program was developed on Linux. I think all the system calls are POSIX 
ones so they should be available on all modern Unix variants (I've not tested 
it on my FreeBSD box yet). 

The server-side hook script uses pysqlite2, which is supposed to be bundled
with Python v2.5 and above. Under Ubuntu (not sure about other distributions)
they can be installed using "apt-get install python-pysqlite2". Subversion 
developmental files are not required for building, though a Subversion server 
and client is needed to use this program.


Building & Installing
=====================
setlabel.py, dellabel.py, listlabels.py and svnwrapper.py need to be placed 
somewhere that is listed on your search path ($PATH in bash) such as /usr/bin 
or /usr/local/bin. prc.py needs to be renamed to pre-revprop-change and then 
placed in the hooks directory within the repository on your server.


Included programs/scripts
=========================
* prc.py         Implements the server-side pre-revprop-change lookup hook.
* setlabel.py    Sets a label.
* dellabel.py    Deletes a label
* listlabels.py  Lists label assignments.
* svnwrapper.py  The wrapper that surrounds svn.
  

Technical details
=================
Label (Tags in CVS terminology) storage is done by attaching an (unversioned) 
revision property (svnlabel:tag) to the revision being tagged. Because 
Subversion doesn't support searching for revision properties, the 
pre-revprop-change hook is used to intercept the relevant tags and (after 
some checks) store them in a database. 

Searching exploits the feature that if a hook returns an error code, any 
output is sent back to the Subversion client for displaying to the user. In 
order to implement label listing (and searching), some revision properties 
are treated as commands, and the relevant data is sent back as an 'error' 
message. Bit of a hack, but it works.

I have considered the alternative possibility of actually storing the labels
using revision properties, rather than just using them as a transport
mechanism. The problem is that an altered pre-revprop-change is required
in all cases, and for large repositories it is inefficent to download the 
entire revision-label mapping list for every client-side lookups.


Limitations
===========
If you already use pre-prevprop-change for other things, you will need to 
rename the one included with this program and then reference it from your
existing pre-prevprop-change script.


Licence
=======
This program is currently licenced under the GPL (see licence.txt). I might
change to a BSD licence if there are sufficent requests.


Contact
=======
Website: http://svnlabel.remynet.org
  Email: remy.horton@bris.ac.uk

