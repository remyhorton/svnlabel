SVN-Label: Subversion revision number aliases
=============================================
This program is a wrapper (and server-side hook) that implements revision
number aliasing. The wrapper rewrites the Subversion command-line, replacing
labels (aliases) with the equivalent revision numbers. Subversion server hooks
are used so that the label mappings are kept on the server and are accessed
via the Subversion connection.


## Background
Quite a few people who have used CVS previously dislike Subversion's idea
that making a Tag is done by making a Branch, and *calling* it a Tag. It has
its merits, but a lot of people who came from CVS find this unintuitive.
Allowing people to give revision numbers aliases as been floated as a near
equivalent to old CVS Tags, for various reasons the suggestion never seemed
to get implemented.

SVN-Lavel was started as a proof-of-concept and indirectly is a back-door
implementation of (unversioned) property searching. Originally intended as
a political statement rather than a production tool, I maintained it for
quite some time as there seemed to be genuine interest in it at the time.


## Current status
SVN-Label is no longer maintained. It has been converted from tarballs into
a repository for historical reference.


## Licence
IconSharp is licenced under [version 3 of the GPL][gpl3].


## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

